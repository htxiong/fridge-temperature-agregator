1. How to run this app?

You should get java 8 installed on your machine.

1) Import it to your IDE (e.g. Intellij) and run App in it.

2) Using maven.
  2.1) Go to project Home and Run: mvn compile package.
  2.2) There will be two jar files created in target folder, one jar file does not contains dependencies and another does.
  2.3) Run: java -jar target/TemperatureAggregator-1.0-SNAPSHOT-jar-with-dependencies.jar inputFilePath


2. Design:

1) App.java is the entry of this application.
2) TemperatureAggregator class is the main application. It provides:
 2.1) a method to read fridge data from a file and store all these data per fridge.
 2.2) also a method to generate a report.
3) Fridge class defines:
 3.1) how the temperature data will be stored.
 3.2) how to calculate average, median and mode temperatures.
 3.3) how to generate a report for fridge.
4) TemperatureRecord defines the data structure of how fridge data be stored.
5) TemperatureReport defines the format of reports for fridge.


3. Assumptions

1) Pass the input data to application by reading from a file.
2) Order of property values of fridges in report is not a matter.
3) No concurrent read/write fridge data is required.


4. Future improvements

1) Improve the time complicity in Fridge.java.
2) Make this application be thread safe.
3) Make the output JSON string preserve the original order.
4) Better tests.