package com.htxiong.temperatureaggregator;

import org.json.simple.JSONObject;

import java.util.Objects;


public class TemperatureRecord implements Comparable<TemperatureRecord> {
    private String id;
    private Long timestamp;
    private Double value;

    public String getId() {
        return id;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public Double getValue() {
        return value;
    }

    public TemperatureRecord(JSONObject json) {
        this.id = (String)getPropertyValue(json,"id");
        this.timestamp = (Long) getPropertyValue(json,"timestamp");
        this.value = (Double)getPropertyValue(json,"temperature");
    }

    private Object getPropertyValue(JSONObject json, String propertyName) {
        Object propertyValue = json.get(propertyName);
        if (propertyValue == null) {
            throw new IllegalArgumentException(String.format("The temperature JSON object provided is not valid, %s was not found in one record.", propertyName));
        }

        return propertyValue;
    }

    @Override
    public int compareTo(TemperatureRecord temperatureRecord) {
        return this.getValue().compareTo(temperatureRecord.getValue());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TemperatureRecord record = (TemperatureRecord) o;
        return Objects.equals(id, record.id) &&
                Objects.equals(timestamp, record.timestamp) &&
                Objects.equals(value, record.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, timestamp, value);
    }
}


