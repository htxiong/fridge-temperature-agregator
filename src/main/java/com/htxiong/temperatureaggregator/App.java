package com.htxiong.temperatureaggregator;


public class App 
{
    public static void main( String[] args )
    {
        if(args.length == 1) {
            String recipesJsonPath = args[0];

            TemperatureAggregator temperatureAggregator = new TemperatureAggregator();
            temperatureAggregator.readSensorData(recipesJsonPath);
            System.out.println(temperatureAggregator.generateReport());
        } else {
            throw new IllegalArgumentException("Please provide 1 argument as input, it is the path of fridge sensors data json file.");
        }
    }
}
