package com.htxiong.temperatureaggregator;

import java.util.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class TemperatureAggregator {
    private Map<String, Fridge> fridges;

    public TemperatureAggregator() {
        this.fridges = new LinkedHashMap<>();
    }

    public Map<String, Fridge> getFridges() {
        return fridges;
    }

    public void readSensorData(String filePath) {
        FileReader reader = null;

        try {
            reader = new FileReader(filePath);

            JSONParser jsonParser = new JSONParser();
            JSONArray recipes = (JSONArray) jsonParser.parse(reader);

            Iterator i = recipes.iterator();
            while (i.hasNext()) {
                JSONObject recipeJson = (JSONObject) i.next();
                TemperatureRecord temperatureRecord = new TemperatureRecord(recipeJson);
                this.recordFridgeTemperatureRecord(temperatureRecord);
            }

        } catch (FileNotFoundException ex) {
            throw new IllegalArgumentException("The temperature sensor data json file you provided is not found.");
        } catch (Exception ex) {
            throw new IllegalArgumentException(ex);
        } finally {
            try {
                if(reader != null) reader.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void recordFridgeTemperatureRecord(TemperatureRecord record) {
        Fridge fridge = this.fridges.get(record.getId());
        if (fridge == null) {
            fridge = new Fridge(record.getId());
            this.fridges.put(fridge.getId(), fridge);
        }
        fridge.recordTemperature(record);
    }

    public String generateReport() {
        JSONArray report = new JSONArray();
        for (Fridge fridge : this.fridges.values()) {
            report.add(fridge.generateReport().toJson());
        }

        return report.toJSONString();
    }
}
