package com.htxiong.temperatureaggregator;

import utils.SortedArrayList;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Fridge {

    private String id;
    private Instant lastModifiedAt = Instant.now();
    private Instant lastReportGeneratedAt = Instant.now();
    private TemperatureReport latestReport;
    private final SortedArrayList<TemperatureRecord> temperatureRecords = new SortedArrayList<>();


    public Fridge(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public int getNumberOfRecords() {
        return this.temperatureRecords.size();
    }

    private void setLatestReport(TemperatureReport report) {
        this.latestReport = report;
        this.lastReportGeneratedAt = Instant.now();
    }

    public void recordTemperature(TemperatureRecord temperatureRecord) {
        if (this.id.equals(temperatureRecord.getId())) {
            this.temperatureRecords.insertSorted(temperatureRecord);
            this.lastModifiedAt = Instant.now();
        } else {
            throw new IllegalArgumentException("The temperature record provided is not belong to this fridge.");
        }
    }

    public TemperatureReport generateReport() {
        if(!isReportUpToDate()) {
            TemperatureReport report = new TemperatureReport(id, calculateAverage(), calculateMedian(),calculateMode());
            this.setLatestReport(report);
        }
        return this.latestReport;
    }

    private Double calculateAverage() {
        double sum = 0;
        for (TemperatureRecord record : this.temperatureRecords) {
            sum += record.getValue();
        }
        double average = this.temperatureRecords.isEmpty() ? 0 : sum / this.temperatureRecords.size();
        return formatDoubleValue(average);
    }

    private List<Double> calculateMode() {
        Map<Double, Integer> temperatureValueCountMap = new LinkedHashMap<>();

        for (TemperatureRecord record : this.temperatureRecords) {
            Integer temperatureValueCount = temperatureValueCountMap.get(record.getValue());
            if (temperatureValueCount == null) {
                temperatureValueCountMap.put(record.getValue(), 1);
            } else {
                temperatureValueCount += 1;
                temperatureValueCountMap.put(record.getValue(), temperatureValueCount);
            }
        }

        Stream<Map.Entry<Double, Integer>> sortedTemperatureValueCountStream = temperatureValueCountMap.entrySet().stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()));

        SortedArrayList<Double> modes = new SortedArrayList<>();
        int maxCount = 0;
        for (Map.Entry<Double, Integer> entry : sortedTemperatureValueCountStream.collect(Collectors.toList())) {
            if (modes.isEmpty()) {
                modes.insertSorted(entry.getKey());
                maxCount = entry.getValue();
            } else if (entry.getValue() == maxCount) {
                modes.insertSorted(entry.getKey());
            }
        }
        return modes;
    }

    private Double calculateMedian() {
        double median = 0;
        int size = this.temperatureRecords.size();
        if (size > 0) {
            median = (size % 2 == 0) ?
                    (this.temperatureRecords.get(size/2).getValue() + this.temperatureRecords.get(size/2 - 1).getValue()) / 2
                    :this.temperatureRecords.get(size/2).getValue();
        }

        return formatDoubleValue(median);
    }

    private Double formatDoubleValue(double value) {
        return Math.round(value * 100.0) / 100.0;
    }

    private boolean isReportUpToDate() {
        boolean hasReportGenerated = this.latestReport != null;
        boolean hasNewRecordsSinceLastReport = this.lastModifiedAt.isAfter(lastReportGeneratedAt);
        return hasReportGenerated && !hasNewRecordsSinceLastReport;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Fridge fridge = (Fridge) o;
        return Objects.equals(id, fridge.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

