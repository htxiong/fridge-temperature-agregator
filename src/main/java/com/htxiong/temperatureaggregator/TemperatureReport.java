package com.htxiong.temperatureaggregator;

import org.json.simple.JSONObject;

import java.util.*;

public class TemperatureReport {
    private String id;
    private Double average;
    private Double median;
    private List<Double> modes;


    public TemperatureReport(String id, Double average, Double median, List<Double> modes) {
        this.id = id;
        this.average = average;
        this.median = median;
        this.modes = modes;
    }

    public JSONObject toJson() {
        Map<String, Object> jsonOrderedMap = new LinkedHashMap<>();
        jsonOrderedMap.put("id", this.id);
        jsonOrderedMap.put("average", this.average);
        jsonOrderedMap.put("median", this.median);
        jsonOrderedMap.put("modes", this.modes);
        return new JSONObject(jsonOrderedMap);
    }
}



