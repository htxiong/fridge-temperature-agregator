package com.htxiong.temperatureaggregator;


import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;


public class AppTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testAppWithTwoArguments() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Please provide 1 argument as input, it is the path of fridge sensors data json file.");

        String[] args = {"data.txt", "data2.txt"};
        App.main(args);
    }


    @Test
    public void testAppWithZeroArguments() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Please provide 1 argument as input, it is the path of fridge sensors data json file.");

        String[] args = {};
        App.main(args);
    }


    @Test
    public void testAppWith1ValidArguments() throws Exception {
        String path = "/data.txt";
        assert getClass().getResource(path) != null;
        URL resourceUrl = getClass().getResource(path);
        Path resourcePath = Paths.get(resourceUrl.toURI());

        String[] args = {resourcePath.toString()};
        App.main(args);
    }
}
