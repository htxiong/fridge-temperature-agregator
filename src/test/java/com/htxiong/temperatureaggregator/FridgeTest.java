package com.htxiong.temperatureaggregator;

import org.json.simple.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.LinkedList;
import java.util.List;


public class FridgeTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    String fridgeId = "a";
    Fridge fridge;

    @Before
    public void setUp() {
        fridge = new Fridge(fridgeId);
    }

    @After
    public void tearDown() {
        fridge = null;
    }

    @Test
    public void testCreateFridge() {
        Fridge expected = new Fridge("a");
        Fridge actual = fridge;

        assert expected.equals(actual);
    }

    @Test
    public void testRecordTemperatureRecordHappyCase() {
        JSONObject objectForRecordA = new JSONObject();
        objectForRecordA.put("id", "a");
        objectForRecordA.put("timestamp", 1509493641L);
        objectForRecordA.put("temperature", 3.53);
        TemperatureRecord recordForA = new TemperatureRecord(objectForRecordA);

        fridge.recordTemperature(recordForA);

        int expectedNumberOfRecords = 1;
        int actualNumberOfRecords = fridge.getNumberOfRecords();
        assert expectedNumberOfRecords == actualNumberOfRecords;
    }

    @Test
    public void testRecordTemperatureRecordSadCase() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("The temperature record provided is not belong to this fridge.");

        JSONObject objectForRecordB = new JSONObject();
        objectForRecordB.put("id", "b");
        objectForRecordB.put("timestamp", 1509493641L);
        objectForRecordB.put("temperature", 4.53);
        TemperatureRecord recordForB = new TemperatureRecord(objectForRecordB);

        fridge.recordTemperature(recordForB);
    }


    @Test
    public void testRecordTemperatureRecordAndGenerateReport() {
        JSONObject objectForRecordA = new JSONObject();
        objectForRecordA.put("id", "a");
        objectForRecordA.put("timestamp", 1509493641L);
        objectForRecordA.put("temperature", 3.45);
        TemperatureRecord recordForA = new TemperatureRecord(objectForRecordA);

        JSONObject objectForRecordB = new JSONObject();
        objectForRecordB.put("id", "a");
        objectForRecordB.put("timestamp", 1509493642L);
        objectForRecordB.put("temperature", 4.55);
        TemperatureRecord recordForB = new TemperatureRecord(objectForRecordB);

        fridge.recordTemperature(recordForA);
        fridge.recordTemperature(recordForB);

        List<Double> modes = new LinkedList<>();
        modes.add(3.45);
        modes.add(4.55);
        TemperatureReport expectedValue = new TemperatureReport("a", 4.00, 4.00, modes);
        TemperatureReport actualValue = fridge.generateReport();

        assert expectedValue.toJson().equals(actualValue.toJson());
    }
}
