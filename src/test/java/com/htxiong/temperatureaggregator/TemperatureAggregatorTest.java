package com.htxiong.temperatureaggregator;

import org.json.simple.JSONArray;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;


public class TemperatureAggregatorTest {
    @Rule
    public ExpectedException thrown= ExpectedException.none();
    public TemperatureAggregator temperatureAggregator;

    @Before
    public void setUp() {
        temperatureAggregator = new TemperatureAggregator();
    }

    @After
    public void tearDown() {
        temperatureAggregator = null;
    }

    @Test
    public void testReadSensorData() throws Exception {
        String path = "/data.txt";
        assert getClass().getResource(path) != null;
        URL resourceUrl = getClass().getResource(path);
        Path resourcePath = Paths.get(resourceUrl.toURI());
        temperatureAggregator.readSensorData(resourcePath.toString());

        int expectedNumberOfFridges = 3;
        int actualNumberOfFridges = temperatureAggregator.getFridges().size();
        assert expectedNumberOfFridges == actualNumberOfFridges;
    }

    @Test
    public void testReadSensorDataWithInvalidValue() throws Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("java.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Double");

        String path = "/data-invalid.txt";
        assert getClass().getResource(path) != null;
        URL resourceUrl = getClass().getResource(path);
        Path resourcePath = Paths.get(resourceUrl.toURI());
        temperatureAggregator.readSensorData(resourcePath.toString());
    }

    @Test
    public void testReadSensorDataWithInvalidPath() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("The temperature sensor data json file you provided is not found.");

        String path = "non-existent-data.txt";
        temperatureAggregator.readSensorData(path);

    }

    @Test
    public void testReadSensorDataAndGenerateReport() throws Exception {
        String path = "/data.txt";
        assert getClass().getResource(path) != null;
        URL resourceUrl = getClass().getResource(path);
        Path resourcePath = Paths.get(resourceUrl.toURI());
        temperatureAggregator.readSensorData(resourcePath.toString());

        List<Double> modesForA = new LinkedList<>();
        modesForA.add(3.53);
        TemperatureReport reportForA = new TemperatureReport("a", 3.78, 3.65, modesForA);

        List<Double> modesForB = new LinkedList<>();
        modesForB.add(4.15);
        TemperatureReport reportForB = new TemperatureReport("b", 4.08, 4.14, modesForB);

        List<Double> modesForC = new LinkedList<>();
        modesForC.add(3.36);
        modesForC.add(3.96);
        TemperatureReport reportForC = new TemperatureReport("c", 3.72, 3.95, modesForC);

        JSONArray expectedReportJson = new JSONArray();
        expectedReportJson.add(reportForA.toJson());
        expectedReportJson.add(reportForB.toJson());
        expectedReportJson.add(reportForC.toJson());
        String expectedReport = expectedReportJson.toJSONString();

        String actualReport = temperatureAggregator.generateReport();

        assert expectedReport.equals(actualReport);
    }
}
