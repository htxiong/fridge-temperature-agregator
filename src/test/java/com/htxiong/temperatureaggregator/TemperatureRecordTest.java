package com.htxiong.temperatureaggregator;

import org.json.simple.JSONObject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


public class TemperatureRecordTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testCreateTemperatureRecordWithoutId() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("The temperature JSON object provided is not valid, id was not found in one record.");

        JSONObject object = new JSONObject();
        object.put("timestamp", 1509493641L);
        object.put("temperature", 3.53);
        new TemperatureRecord(object);
    }

    @Test
    public void testCreateTemperatureRecordWithoutTimestamp() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("The temperature JSON object provided is not valid, timestamp was not found in one record.");

        JSONObject object = new JSONObject();
        object.put("id", "a");
        object.put("temperature", 3.53);
        new TemperatureRecord(object);
    }

    @Test
    public void testCreateTemperatureRecordWithoutTemperature() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("The temperature JSON object provided is not valid, temperature was not found in one record.");

        JSONObject object = new JSONObject();
        object.put("id", "a");
        object.put("timestamp", 1509493641L);
        new TemperatureRecord(object);
    }

    @Test
    public void testCreateTemperatureRecordWithValidInputs() {
        String expectedId = "a";
        Long expectedTimestamp = 1509493641L;
        Double expectedTemperature = 3.53;

        JSONObject object = new JSONObject();
        object.put("id", "a");
        object.put("timestamp", 1509493641L);
        object.put("temperature", 3.53);
        TemperatureRecord actualRecord = new TemperatureRecord(object);

        assert expectedId.equals(actualRecord.getId());
        assert expectedTimestamp.equals(actualRecord.getTimestamp());
        assert expectedTemperature.equals(actualRecord.getValue());
    }

    @Test
    public void testCompareToWithTwoNotEquivalentRecords() {
        JSONObject objectForRecordA = new JSONObject();
        objectForRecordA.put("id", "a");
        objectForRecordA.put("timestamp", 1509493641L);
        objectForRecordA.put("temperature", 3.53);
        TemperatureRecord actualRecordForA = new TemperatureRecord(objectForRecordA);

        JSONObject objectForRecordB = new JSONObject();
        objectForRecordB.put("id", "b");
        objectForRecordB.put("timestamp", 1509493641L);
        objectForRecordB.put("temperature", 4.53);
        TemperatureRecord actualRecordForB = new TemperatureRecord(objectForRecordB);

        assert actualRecordForA.compareTo(actualRecordForB) == -1;
        assert actualRecordForB.compareTo(actualRecordForA) == 1;
    }

    @Test
    public void testCompareToWithTwoEquivalentRecords() {
        JSONObject objectForRecordA = new JSONObject();
        objectForRecordA.put("id", "a");
        objectForRecordA.put("timestamp", 1509493641L);
        objectForRecordA.put("temperature", 3.53);
        TemperatureRecord actualRecordForA = new TemperatureRecord(objectForRecordA);

        JSONObject objectForRecordB = new JSONObject();
        objectForRecordB.put("id", "b");
        objectForRecordB.put("timestamp", 1509493641L);
        objectForRecordB.put("temperature", 3.53);
        TemperatureRecord actualRecordForB = new TemperatureRecord(objectForRecordB);

        assert actualRecordForA.compareTo(actualRecordForB) == 0;
        assert actualRecordForB.compareTo(actualRecordForA) == 0;
    }


    @Test
    public void testCompareToWithSameRecord() {
        JSONObject objectForRecordA = new JSONObject();
        objectForRecordA.put("id", "a");
        objectForRecordA.put("timestamp", 1509493641L);
        objectForRecordA.put("temperature", 3.53);
        TemperatureRecord actualRecordForA = new TemperatureRecord(objectForRecordA);

        assert actualRecordForA.compareTo(actualRecordForA) == 0;
    }
}
